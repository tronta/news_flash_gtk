use crate::app::App;
use crate::error::NewsFlashGtkError;
use crate::i18n::{i18n, i18n_f};
use crate::util::GtkUtil;
use eyre::{eyre, Result};
use glib::{clone, signal::SignalHandlerId, subclass};
use gtk4::{prelude::*, subclass::prelude::*};
use gtk4::{Button, CompositeTemplate, Image, Label, Revealer, Stack};
use libadwaita::{prelude::*, ComboRow, EntryRow, Toast, ToastOverlay};
use news_flash::error::{FeedApiError, NewsFlashError};
use news_flash::models::{
    BasicAuth, DirectLogin, DirectLoginGUI, LoginData, LoginGUI, PasswordLogin as PasswordLoginData, PluginID,
    PluginInfo, TokenLogin,
};
use std::cell::RefCell;

mod imp {
    use super::*;

    #[derive(Debug, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/login/password.blp")]
    pub struct PasswordLogin {
        #[template_child]
        pub logo: TemplateChild<Image>,
        #[template_child]
        pub headline: TemplateChild<Label>,
        #[template_child]
        pub url_entry: TemplateChild<EntryRow>,
        #[template_child]
        pub user_entry: TemplateChild<EntryRow>,
        #[template_child]
        pub pass_entry: TemplateChild<EntryRow>,
        #[template_child]
        pub token_entry: TemplateChild<EntryRow>,
        #[template_child]
        pub http_user_entry: TemplateChild<EntryRow>,
        #[template_child]
        pub http_pass_entry: TemplateChild<EntryRow>,
        #[template_child]
        pub http_revealer: TemplateChild<Revealer>,
        #[template_child]
        pub login_button: TemplateChild<Button>,
        #[template_child]
        pub login_stack: TemplateChild<Stack>,
        #[template_child]
        pub toast_overlay: TemplateChild<ToastOverlay>,
        #[template_child]
        pub login_type_row: TemplateChild<ComboRow>,

        pub url_entry_signal: RefCell<Option<SignalHandlerId>>,
        pub user_entry_signal: RefCell<Option<SignalHandlerId>>,
        pub pass_entry_signal: RefCell<Option<SignalHandlerId>>,
        pub token_entry_signal: RefCell<Option<SignalHandlerId>>,
        pub http_user_entry_signal: RefCell<Option<SignalHandlerId>>,
        pub http_pass_entry_signal: RefCell<Option<SignalHandlerId>>,
        pub login_button_signal: RefCell<Option<SignalHandlerId>>,

        pub plugin_id: RefCell<Option<PluginID>>,
    }

    impl Default for PasswordLogin {
        fn default() -> Self {
            Self {
                logo: TemplateChild::default(),
                headline: TemplateChild::default(),
                url_entry: TemplateChild::default(),
                user_entry: TemplateChild::default(),
                pass_entry: TemplateChild::default(),
                token_entry: TemplateChild::default(),
                http_user_entry: TemplateChild::default(),
                http_pass_entry: TemplateChild::default(),
                http_revealer: TemplateChild::default(),
                login_button: TemplateChild::default(),
                login_stack: TemplateChild::default(),
                toast_overlay: TemplateChild::default(),
                login_type_row: TemplateChild::default(),

                url_entry_signal: RefCell::new(None),
                user_entry_signal: RefCell::new(None),
                pass_entry_signal: RefCell::new(None),
                token_entry_signal: RefCell::new(None),
                http_user_entry_signal: RefCell::new(None),
                http_pass_entry_signal: RefCell::new(None),
                login_button_signal: RefCell::new(None),

                plugin_id: RefCell::new(None),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for PasswordLogin {
        const NAME: &'static str = "PasswordLogin";
        type ParentType = gtk4::Box;
        type Type = super::PasswordLogin;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for PasswordLogin {}

    impl WidgetImpl for PasswordLogin {}

    impl BoxImpl for PasswordLogin {}
}

glib::wrapper! {
    pub struct PasswordLogin(ObjectSubclass<imp::PasswordLogin>)
        @extends gtk4::Widget, gtk4::Box;
}

impl Default for PasswordLogin {
    fn default() -> Self {
        Self::new()
    }
}

impl PasswordLogin {
    pub fn new() -> Self {
        glib::Object::new::<Self>()
    }

    pub fn set_service(&self, info: &PluginInfo) -> Result<()> {
        let imp = self.imp();

        imp.plugin_id.replace(Some(info.id.clone()));

        self.reset();

        // set Icon
        if let Some(icon) = info.icon.clone() {
            let bytes = glib::Bytes::from_owned(icon.into_data());
            let texture = gdk4::Texture::from_bytes(&bytes);
            imp.logo.set_from_paintable(texture.ok().as_ref());
        }

        imp.login_type_row
            .connect_selected_notify(clone!(@weak self as page => @default-panic, move |combo_row| {
                let show_password = combo_row.selected() == 0;
                let show_token = combo_row.selected() == 1;

                let imp = page.imp();
                imp.user_entry.set_visible(show_password);
                imp.pass_entry.set_visible(show_password);
                imp.token_entry.set_visible(show_token);
            }));

        // set headline
        imp.headline.set_text(&i18n_f("Log into {}", &[&info.name]));

        if let LoginGUI::Direct(direct_login_desc) = &info.login_gui {
            // show/hide url & http-auth fields
            imp.url_entry.set_visible(direct_login_desc.url);
            imp.http_revealer.set_reveal_child(false);

            // set focus to first entry
            if direct_login_desc.url {
                imp.url_entry.grab_focus();
            } else {
                imp.user_entry.grab_focus();
            }

            // show/hide login type selector
            imp.login_type_row.set_visible(direct_login_desc.support_token_login);

            // check if 'login' should be clickable
            GtkUtil::disconnect_signal(imp.url_entry_signal.take(), &*imp.url_entry);
            GtkUtil::disconnect_signal(imp.user_entry_signal.take(), &*imp.user_entry);
            GtkUtil::disconnect_signal(imp.pass_entry_signal.take(), &*imp.pass_entry);
            GtkUtil::disconnect_signal(imp.token_entry_signal.take(), &*imp.token_entry);
            GtkUtil::disconnect_signal(imp.http_user_entry_signal.take(), &*imp.http_user_entry);
            GtkUtil::disconnect_signal(imp.http_pass_entry_signal.take(), &*imp.http_pass_entry);

            imp.url_entry_signal
                .replace(Some(self.setup_entry(&imp.url_entry, direct_login_desc)));
            imp.user_entry_signal
                .replace(Some(self.setup_entry(&imp.user_entry, direct_login_desc)));
            imp.pass_entry_signal
                .replace(Some(self.setup_entry(&imp.pass_entry, direct_login_desc)));
            imp.token_entry_signal
                .replace(Some(self.setup_entry(&imp.token_entry, direct_login_desc)));
            imp.http_user_entry_signal
                .replace(Some(self.setup_entry(&imp.http_user_entry, direct_login_desc)));
            imp.http_pass_entry_signal
                .replace(Some(self.setup_entry(&imp.http_pass_entry, direct_login_desc)));

            // harvest login data
            GtkUtil::disconnect_signal(imp.login_button_signal.take(), &*imp.login_button);

            imp.login_button_signal
                .replace(Some(imp.login_button.connect_clicked(clone!(
                @weak self as page,
                @strong direct_login_desc,
                @strong info.id as plugin_id => @default-panic, move |button|
            {
                let imp = page.imp();
                imp.login_stack.set_visible_child_name("login_spinner");
                button.set_sensitive(false);

                let url: Option<String> = if direct_login_desc.url {
                    let mut url_text = imp.url_entry.text().as_str().to_owned();
                    if !url_text.starts_with("http://") && !url_text.starts_with("https://") {
                        url_text.insert_str(0, "https://");
                    }
                    imp.url_entry.set_text(&url_text);

                    Some(url_text)
                } else {
                    None
                };
                let user = imp.user_entry
                    .text()
                    .as_str()
                    .to_owned();
                let password = imp.pass_entry
                    .text()
                    .as_str()
                    .to_owned();
                let token = imp.token_entry
                    .text()
                    .as_str()
                    .to_owned();

                let basic_auth = if imp.http_revealer.is_child_revealed() && !imp.http_user_entry.text().is_empty() {
                    Some(BasicAuth {
                        user: imp.http_user_entry.text().as_str().to_owned(),
                        password: if imp.http_pass_entry.text().is_empty() {
                            None
                        } else {
                            Some(imp.http_pass_entry.text().as_str().to_owned())
                        },
                    })
                } else {
                    None
                };

                let login_data = if imp.login_type_row.selected() == 0 {
                    LoginData::Direct(DirectLogin::Password(PasswordLoginData {
                        id: plugin_id.clone(),
                        url,
                        user,
                        password,
                        basic_auth,
                    }))
                } else {
                    LoginData::Direct(DirectLogin::Token(TokenLogin {
                        id: plugin_id.clone(),
                        url,
                        token,
                        basic_auth,
                    }))
                };

                App::default().login(login_data);
            }))));

            return Ok(());
        }

        Err(eyre!("No login GUI description"))
    }

    pub fn reset(&self) {
        let imp = self.imp();

        imp.login_stack.set_visible_child_name("login_label");
        imp.login_button.set_sensitive(false);
        imp.url_entry.set_text("");
        imp.user_entry.set_text("");
        imp.pass_entry.set_text("");
        imp.token_entry.set_text("");
        imp.http_user_entry.set_text("");
        imp.http_pass_entry.set_text("");

        imp.user_entry.set_visible(true);
        imp.pass_entry.set_visible(true);
        imp.token_entry.set_visible(false);

        imp.login_type_row.set_selected(0);
    }

    pub fn show_error(&self, error: NewsFlashError) {
        let imp = self.imp();

        imp.login_stack.set_visible_child_name("login_label");
        imp.login_button.set_sensitive(true);

        let message = match &error {
            NewsFlashError::API(api_err) => match &api_err {
                FeedApiError::Auth => {
                    imp.http_revealer.set_reveal_child(true);

                    let message = i18n("Unauthorized");
                    log::warn!("{}", message);

                    let toast = Toast::new(&message);
                    imp.toast_overlay.add_toast(toast);
                    message
                }
                FeedApiError::Network(error) => {
                    let message = if let Some(status) = error.status() {
                        if status.as_u16() == 495 || status.as_u16() == 496 {
                            let message = i18n("No valid CA certificate available");
                            log::warn!("{}", message);

                            let toast = Toast::new(&message);
                            toast.set_button_label(Some(&i18n("ignore future TLS errors")));
                            toast.set_action_name(Some("win.ignore-tls-errors"));
                            imp.toast_overlay.add_toast(toast);
                            Some(message)
                        } else {
                            None
                        }
                    } else {
                        None
                    };

                    if let Some(message) = message {
                        message
                    } else {
                        let message = i18n("Network Error");
                        log::warn!("{}", message);

                        let toast = Toast::new(&message);
                        toast.set_button_label(Some(&i18n("details")));
                        toast.set_action_name(Some("win.show-error-dialog"));
                        imp.toast_overlay.add_toast(toast);
                        message
                    }
                }
                _ => {
                    let message = i18n("Could not log in");
                    log::warn!("{}", message);

                    let toast = Toast::new(&message);
                    toast.set_button_label(Some(&i18n("details")));
                    toast.set_action_name(Some("win.show-error-dialog"));
                    imp.toast_overlay.add_toast(toast);
                    message
                }
            },
            _ => {
                let message = i18n("Unknown error");
                log::warn!("{}", message);

                let toast = Toast::new(&message);
                toast.set_button_label(Some(&i18n("details")));
                toast.set_action_name(Some("win.show-error-dialog"));
                imp.toast_overlay.add_toast(toast);
                message
            }
        };

        App::default().set_newsflash_error(NewsFlashGtkError::NewsFlash {
            source: error,
            context: message,
        });
    }

    pub fn fill(&self, data: DirectLogin) {
        let imp = self.imp();

        imp.url_entry.set_text("");
        imp.user_entry.set_text("");
        imp.pass_entry.set_text("");
        imp.token_entry.set_text("");
        imp.http_user_entry.set_text("");
        imp.http_pass_entry.set_text("");

        if let DirectLogin::Password(password_data) = &data {
            imp.login_type_row.set_selected(0);
            self.fill_url(password_data.url.as_deref());
            imp.user_entry.set_text(&password_data.user);
            imp.pass_entry.set_text(&password_data.password);
            self.fill_basic_auth(password_data.basic_auth.as_ref());
        } else if let DirectLogin::Token(token_data) = &data {
            imp.login_type_row.set_selected(1);
            self.fill_url(token_data.url.as_deref());
            imp.token_entry.set_text(&token_data.token);
            self.fill_basic_auth(token_data.basic_auth.as_ref());
        }
    }

    fn fill_url(&self, url: Option<&str>) {
        if let Some(url) = &url {
            let imp = self.imp();
            imp.url_entry.set_text(url);
        }
    }

    fn fill_basic_auth(&self, basic_auth: Option<&BasicAuth>) {
        let imp = self.imp();

        if let Some(basic_auth) = basic_auth {
            imp.http_user_entry.set_text(&basic_auth.user);
            imp.http_revealer.set_reveal_child(true);

            if let Some(password) = basic_auth.password.as_deref() {
                imp.http_pass_entry.set_text(password);
            }
        }
    }

    fn setup_entry(&self, entry: &EntryRow, gui_desc: &DirectLoginGUI) -> SignalHandlerId {
        entry.connect_text_notify(clone!(
            @weak self as page,
            @strong gui_desc => @default-panic, move |_entry|
        {
            if gui_desc.url && GtkUtil::is_entry_row_emty(&page.imp().url_entry) {
                page.imp().login_button.set_sensitive(false);
                return;
            }
            if gui_desc.support_token_login && page.imp().login_type_row.selected() == 1 {
                if GtkUtil::is_entry_row_emty(&page.imp().token_entry) {
                    page.imp().login_button.set_sensitive(false);
                    return;
                }
            } else if GtkUtil::is_entry_row_emty(&page.imp().user_entry) || GtkUtil::is_entry_row_emty(&page.imp().pass_entry) {
                page.imp().login_button.set_sensitive(false);
                return;
            }

            page.imp().login_button.set_sensitive(true);
        }))
    }
}
