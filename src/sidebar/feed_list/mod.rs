pub mod item_row;
pub mod item_row_dnd;
pub mod models;

use crate::app::App;
use crate::sidebar::feed_list::{
    item_row::ItemRow,
    models::{EditedFeedListItem, FeedListItem, FeedListItemGObject, FeedListItemID, FeedListTree},
};
use crate::sidebar::SidebarIterateItem;
use diffus::{
    edit::enm::Edit as EnumEdit,
    edit::{collection, Edit},
    Diffable,
};
use gio::ListStore;
use glib::clone;
use gtk4::{
    prelude::*, subclass::prelude::*, Box, CompositeTemplate, CustomFilter, FilterChange, FilterListModel, ListView,
    ScrolledWindow, SignalListItemFactory, SingleSelection, TreeListModel, Widget,
};
use gtk4::{ConstantExpression, ListItem, PropertyExpression, TreeListRow};
use news_flash::models::CategoryID;
use std::cell::RefCell;
use std::collections::HashMap;

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/sidebar/feed_list.blp")]
    pub struct FeedList {
        #[template_child]
        pub listview: TemplateChild<ListView>,
        #[template_child]
        pub factory: TemplateChild<SignalListItemFactory>,
        #[template_child]
        pub selection: TemplateChild<SingleSelection>,
        #[template_child]
        pub filter_model: TemplateChild<FilterListModel>,
        #[template_child]
        pub filter: TemplateChild<CustomFilter>,

        pub tree_model: TreeListModel,
        pub list_store: ListStore,

        pub scroll: RefCell<Option<ScrolledWindow>>,
        pub item_index: RefCell<HashMap<FeedListItemID, FeedListItemGObject>>,
        pub tree: RefCell<FeedListTree>,
    }

    impl Default for FeedList {
        fn default() -> Self {
            let list_store = ListStore::new::<FeedListItemGObject>();
            let tree_model = TreeListModel::new(list_store.clone(), false, false, |item| {
                let item_gobject = item.downcast_ref::<FeedListItemGObject>().unwrap();
                item_gobject.children_model()
            });

            Self {
                listview: TemplateChild::default(),
                factory: TemplateChild::default(),
                selection: TemplateChild::default(),
                filter_model: TemplateChild::default(),
                filter: TemplateChild::default(),

                tree_model,
                list_store,

                scroll: RefCell::new(None),
                item_index: RefCell::new(HashMap::new()),
                tree: RefCell::new(FeedListTree::new()),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for FeedList {
        const NAME: &'static str = "FeedList";
        type ParentType = Box;
        type Type = super::FeedList;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for FeedList {}

    impl WidgetImpl for FeedList {}

    impl BoxImpl for FeedList {}
}

glib::wrapper! {
    pub struct FeedList(ObjectSubclass<imp::FeedList>)
        @extends Widget, Box;
}

impl Default for FeedList {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl FeedList {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn init(&self, sidebar_scroll: &ScrolledWindow) {
        let imp = self.imp();
        imp.scroll.replace(Some(sidebar_scroll.clone()));
        imp.filter.set_filter_func(|obj| {
            let tree_list_row = obj
                .downcast_ref::<TreeListRow>()
                .expect("The object needs to be of type `TreeListRow`.");

            let item_gobject = tree_list_row
                .item()
                .expect("No item in TreeListRow")
                .downcast::<FeedListItemGObject>()
                .expect("The object needs to be of type `FeedListItemGObject`.");

            let item_count = item_gobject.item_count();

            // Only allow even numbers
            !App::default().settings().read().get_feed_list_only_show_relevant() || item_count > 0
        });
        imp.filter_model.set_model(Some(&imp.tree_model));

        imp.factory.connect_setup(
            clone!(@weak self as this => @default-panic, move |_factory, list_item| {
                let row = ItemRow::new();

                let list_item = list_item.downcast_ref::<ListItem>().unwrap();
                list_item.set_child(Some(&row));

                row.connect_local("activated", false, clone!(@weak this => @default-panic, move |_args| {
                    let imp = this.imp();
                    imp.listview.emit_by_name::<()>("activate", &[&imp.selection.selected()]);
                    None
                }));

                row.connect_destroy(|row| row.teardown_row());

                // Create expression describing `list_item->item`
                let list_item_expression = ConstantExpression::new(list_item);
                let item_gobject_expression =
                    PropertyExpression::new(ListItem::static_type(), Some(&list_item_expression), "item");

                let item2_gobject_expression =
                    PropertyExpression::new(TreeListRow::static_type(), Some(&item_gobject_expression), "item");

                // Update item count
                let count_expression = PropertyExpression::new(
                    FeedListItemGObject::static_type(),
                    Some(&item2_gobject_expression),
                    "item-count",
                );
                count_expression.bind(&row, "item-count", Some(&row));

                // Update label
                let label_expression = PropertyExpression::new(
                    FeedListItemGObject::static_type(),
                    Some(&item2_gobject_expression),
                    "label",
                );
                label_expression.bind(&row, "label", Some(&row));

                // Update error message
                let error_message_expression = PropertyExpression::new(
                    FeedListItemGObject::static_type(),
                    Some(&item2_gobject_expression),
                    "error-message",
                );
                error_message_expression.bind(&row, "error-message", Some(&row));
            }),
        );
        imp.factory.connect_bind(
            clone!(@weak self as this => @default-panic, move |_factory, list_item| {
                let list_item = list_item.downcast_ref::<ListItem>().unwrap();

                if let Some(item) = list_item.item() {
                    let tree_list_row = item.downcast::<TreeListRow>().unwrap();
                    tree_list_row.connect_expanded_notify(clone!(@weak this => @default-panic, move |tree_list_row| {
                        let imp = this.imp();
                        if let Some(item) = tree_list_row.item() {
                            if let Ok(item_gobject) = item.downcast::<FeedListItemGObject>() {
                                if let FeedListItemID::Category(category_id) = &item_gobject.id() {
                                    imp.tree.borrow_mut().set_category_expanded(category_id, tree_list_row.is_expanded());
                                }
                            }
                        }
                    }));

                    if let Some(item) = tree_list_row.item() {
                        let item = item.downcast::<FeedListItemGObject>().unwrap();
                        if let Some(child) = list_item.child() {
                            let child = child.downcast::<ItemRow>().unwrap();
                            child.bind_model(&item, &tree_list_row);
                        }
                    }
                }
            }),
        );

        imp.factory.connect_unbind(|_factory, list_item| {
            let list_item = list_item.downcast_ref::<ListItem>().unwrap();
            let child = list_item.child().and_downcast::<ItemRow>().unwrap();
            child.unbind_model();
        });
    }

    pub fn listview(&self) -> &ListView {
        let imp = self.imp();
        &imp.listview
    }

    pub fn selection(&self) -> &SingleSelection {
        let imp = self.imp();
        &imp.selection
    }

    pub fn update(&self, new_tree: FeedListTree) {
        let imp = self.imp();

        let mut new_tree = new_tree;
        let mut old_tree = FeedListTree::new();

        std::mem::swap(&mut old_tree, &mut *imp.tree.borrow_mut());

        let expanded_categories = old_tree.get_expanded_categories();
        new_tree.apply_expanded_categories(&expanded_categories);

        let diff = old_tree.top_level.diff(&new_tree.top_level);

        self.process_diff(&diff, &imp.list_store);

        std::mem::swap(&mut new_tree, &mut *imp.tree.borrow_mut());
        imp.filter.changed(FilterChange::Different);
    }

    fn process_diff(&self, diff: &Edit<'_, Vec<FeedListItem>>, list_store: &ListStore) {
        let imp = self.imp();
        let mut pos = 0;

        match diff {
            Edit::Copy(_list) => {
                // no difference
            }
            Edit::Change(diff) => {
                let _ = diff
                    .iter()
                    .map(|edit| {
                        match edit {
                            collection::Edit::Copy(_item) => {
                                // nothing changed
                                pos += 1;
                            }
                            collection::Edit::Insert(item) => {
                                match item {
                                    FeedListItem::Feed(feed_item) => {
                                        let item_gobject = FeedListItemGObject::from_feed(feed_item);
                                        list_store.insert(pos, &item_gobject);
                                        imp.item_index.borrow_mut().insert(item_gobject.id(), item_gobject);
                                    }
                                    FeedListItem::Category(category_item) => {
                                        let item_gobject = FeedListItemGObject::from_category(
                                            category_item,
                                            &mut imp.item_index.borrow_mut(),
                                        );

                                        list_store.insert(pos, &item_gobject);
                                        imp.item_index.borrow_mut().insert(item_gobject.id(), item_gobject);
                                    }
                                }
                                pos += 1;
                            }
                            collection::Edit::Remove(item) => match item {
                                FeedListItem::Feed(feed_item) => {
                                    list_store.remove(pos);
                                    imp.item_index.borrow_mut().remove(&feed_item.id);
                                }
                                FeedListItem::Category(category_item) => {
                                    list_store.remove(pos);
                                    imp.item_index.borrow_mut().remove(&category_item.id);
                                }
                            },
                            collection::Edit::Change(diff) => {
                                match diff {
                                    EnumEdit::VariantChanged(_, _) => {}
                                    EnumEdit::Copy(_item) => {}
                                    EnumEdit::AssociatedChanged(enum_diff) => match enum_diff {
                                        EditedFeedListItem::Feed(changed_feed) => match changed_feed {
                                            Edit::Copy(_feed_item) => {}
                                            Edit::Change(feed_diff) => {
                                                if let Some(item_gobject) = imp.item_index.borrow().get(&feed_diff.id) {
                                                    if let Some(new_label) = &feed_diff.label {
                                                        item_gobject.set_label(new_label);
                                                    }
                                                    if let Some(new_count) = feed_diff.item_count {
                                                        item_gobject.set_item_count(new_count as u32);
                                                    }
                                                    if let Some(error_message) = &feed_diff.error_message {
                                                        item_gobject.set_error_message(error_message);
                                                    }
                                                }
                                            }
                                        },
                                        EditedFeedListItem::Category(changed_category) => match changed_category {
                                            Edit::Copy(_category_item) => {}
                                            Edit::Change(category_diff) => {
                                                let item = imp.item_index.borrow().get(&category_diff.id).cloned();
                                                if let Some(item_gobject) = item {
                                                    if let Some(new_label) = &category_diff.label {
                                                        item_gobject.set_label(new_label);
                                                    }
                                                    if let Some(new_count) = category_diff.item_count {
                                                        item_gobject.set_item_count(new_count as u32);
                                                    }
                                                    if let Some(child_list_store) = item_gobject.children_list_store() {
                                                        self.process_diff(&category_diff.child_diff, &child_list_store);
                                                    }
                                                }
                                            }
                                        },
                                    },
                                }
                                pos += 1;
                            }
                        }
                    })
                    .collect::<Vec<_>>();
            }
        };
    }

    pub fn expand_category(&self, category_id: &CategoryID) {
        let imp = self.imp();
        let item_id = FeedListItemID::Category(category_id.clone());
        let pos = imp.tree.borrow_mut().get_item_pos(item_id);

        if let Some(pos) = pos {
            if let Some(row) = imp.selection.item(pos) {
                if let Ok(tree_list_row) = row.downcast::<TreeListRow>() {
                    tree_list_row.set_expanded(true);
                }
            }
        }
    }

    pub fn expand_collapse_selected_category(&self) {
        let imp = self.imp();

        if let Some(row) = imp.selection.selected_item() {
            if let Ok(tree_list_row) = row.downcast::<TreeListRow>() {
                let is_expanded = tree_list_row.is_expanded();
                tree_list_row.set_expanded(!is_expanded);
            }
        }
    }

    pub fn get_selection(&self) -> Option<(FeedListItemID, String, u32)> {
        let imp = self.imp();

        imp.selection.selected_item().map(|obj| {
            let item = obj.downcast::<TreeListRow>().unwrap().item().unwrap();
            let item_gobject = item.downcast::<FeedListItemGObject>().unwrap();
            (
                item_gobject.id(),
                item_gobject.label().to_string(),
                item_gobject.item_count(),
            )
        })
    }

    pub fn get_first_item(&self) -> Option<FeedListItemID> {
        let imp = self.imp();
        imp.tree.borrow_mut().get_first()
    }

    pub fn get_last_item(&self) -> Option<FeedListItemID> {
        let imp = self.imp();
        imp.tree.borrow_mut().get_last()
    }

    pub fn set_selection(&self, selection: FeedListItemID) {
        let imp = self.imp();
        if let Some(pos) = imp.tree.borrow_mut().get_item_pos(selection) {
            imp.selection.set_selected(pos);
        }
    }

    pub fn calc_next_item(&self) -> SidebarIterateItem {
        let imp = self.imp();
        let item = imp
            .selection
            .selected_item()
            .and_then(|row| row.downcast::<TreeListRow>().ok())
            .and_then(|tree_list_row| tree_list_row.item())
            .and_then(|item| item.downcast::<FeedListItemGObject>().ok());

        if let Some(item) = item {
            imp.tree.borrow_mut().calculate_next_item(item.id())
        } else {
            SidebarIterateItem::NothingSelected
        }
    }

    pub fn calc_prev_item(&self) -> SidebarIterateItem {
        let imp = self.imp();
        let item = imp
            .selection
            .selected_item()
            .and_then(|row| row.downcast::<TreeListRow>().ok())
            .and_then(|tree_list_row| tree_list_row.item())
            .and_then(|item| item.downcast::<FeedListItemGObject>().ok());

        if let Some(item) = item {
            imp.tree.borrow_mut().calculate_prev_item(item.id())
        } else {
            SidebarIterateItem::NothingSelected
        }
    }
}
