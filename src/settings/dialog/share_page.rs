use crate::app::App;
use crate::i18n::i18n;
use glib::{clone, subclass, Propagation};
use gtk4::{prelude::*, subclass::prelude::*, CompositeTemplate, Switch, Widget};
use libadwaita::{subclass::prelude::*, EntryRow, PreferencesPage};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/settings/share.blp")]
    pub struct SettingsSharePage {
        #[template_child]
        pub share_pocket_switch: TemplateChild<Switch>,
        #[template_child]
        pub share_instapaper_switch: TemplateChild<Switch>,
        #[template_child]
        pub share_twitter_switch: TemplateChild<Switch>,
        #[template_child]
        pub share_mastodon_switch: TemplateChild<Switch>,
        #[template_child]
        pub share_reddit_switch: TemplateChild<Switch>,
        #[template_child]
        pub share_telegram_switch: TemplateChild<Switch>,
        #[template_child]
        pub share_clipboard_switch: TemplateChild<Switch>,

        #[template_child]
        pub share_custom_switch: TemplateChild<Switch>,
        #[template_child]
        pub share_custom_name_entry: TemplateChild<EntryRow>,
        #[template_child]
        pub share_custom_url_entry: TemplateChild<EntryRow>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SettingsSharePage {
        const NAME: &'static str = "SettingsSharePage";
        type ParentType = PreferencesPage;
        type Type = super::SettingsSharePage;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for SettingsSharePage {
        fn constructed(&self) {
            self.obj().init();
        }
    }

    impl WidgetImpl for SettingsSharePage {}

    impl PreferencesPageImpl for SettingsSharePage {}
}

glib::wrapper! {
    pub struct SettingsSharePage(ObjectSubclass<imp::SettingsSharePage>)
        @extends Widget, PreferencesPage;
}

impl Default for SettingsSharePage {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl SettingsSharePage {
    pub fn new() -> Self {
        Self::default()
    }

    fn init(&self) {
        let imp = self.imp();

        imp.share_pocket_switch
            .set_active(App::default().settings().read().get_share_pocket_enabled());
        imp.share_instapaper_switch
            .set_active(App::default().settings().read().get_share_instapaper_enabled());
        imp.share_twitter_switch
            .set_active(App::default().settings().read().get_share_twitter_enabled());
        imp.share_mastodon_switch
            .set_active(App::default().settings().read().get_share_mastodon_enabled());
        imp.share_reddit_switch
            .set_active(App::default().settings().read().get_share_reddit_enabled());
        imp.share_telegram_switch
            .set_active(App::default().settings().read().get_share_telegram_enabled());
        imp.share_clipboard_switch
            .set_active(App::default().settings().read().get_share_clipboard_enabled());

        let custom_share_enabled = App::default().settings().read().get_share_custom_enabled();
        imp.share_custom_switch.set_active(custom_share_enabled);
        imp.share_custom_name_entry.set_sensitive(custom_share_enabled);
        imp.share_custom_url_entry.set_sensitive(custom_share_enabled);

        if let Some(name) = App::default().settings().read().get_share_custom_name() {
            imp.share_custom_name_entry.set_text(name);
        }
        if let Some(url) = App::default().settings().read().get_share_custom_url() {
            imp.share_custom_url_entry.set_text(url);
        }

        imp.share_pocket_switch.connect_state_set(|_switch, is_set| {
            if App::default()
                .settings()
                .write()
                .set_share_pocket_enabled(is_set)
                .is_err()
            {
                App::default().in_app_notifiaction(&i18n("Failed to set setting 'pocket enabled'"));
            } else {
                App::default()
                    .main_window()
                    .content_page()
                    .articleview_column()
                    .update_share_popover();
            }
            Propagation::Proceed
        });

        imp.share_instapaper_switch.connect_state_set(|_switch, is_set| {
            if App::default()
                .settings()
                .write()
                .set_share_instapaper_enabled(is_set)
                .is_err()
            {
                App::default().in_app_notifiaction(&i18n("Failed to set setting 'instapaper enabled'"));
            } else {
                App::default()
                    .main_window()
                    .content_page()
                    .articleview_column()
                    .update_share_popover();
            }
            Propagation::Proceed
        });

        imp.share_twitter_switch.connect_state_set(|_switch, is_set| {
            if App::default()
                .settings()
                .write()
                .set_share_twitter_enabled(is_set)
                .is_err()
            {
                App::default().in_app_notifiaction(&i18n("Failed to set setting 'twitter enabled'"));
            } else {
                App::default()
                    .main_window()
                    .content_page()
                    .articleview_column()
                    .update_share_popover();
            }
            Propagation::Proceed
        });

        imp.share_mastodon_switch.connect_state_set(|_switch, is_set| {
            if App::default()
                .settings()
                .write()
                .set_share_mastodon_enabled(is_set)
                .is_err()
            {
                App::default().in_app_notifiaction(&i18n("Failed to set setting 'mastodon enabled'"));
            } else {
                App::default()
                    .main_window()
                    .content_page()
                    .articleview_column()
                    .update_share_popover();
            }
            Propagation::Proceed
        });

        imp.share_reddit_switch.connect_state_set(|_switch, is_set| {
            if App::default()
                .settings()
                .write()
                .set_share_reddit_enabled(is_set)
                .is_err()
            {
                App::default().in_app_notifiaction(&i18n("Failed to set setting 'reddit enabled'"));
            } else {
                App::default()
                    .main_window()
                    .content_page()
                    .articleview_column()
                    .update_share_popover();
            }
            Propagation::Proceed
        });

        imp.share_telegram_switch.connect_state_set(|_switch, is_set| {
            if App::default()
                .settings()
                .write()
                .set_share_telegram_enabled(is_set)
                .is_err()
            {
                App::default().in_app_notifiaction(&i18n("Failed to set setting 'telegram enabled'"));
            } else {
                App::default()
                    .main_window()
                    .content_page()
                    .articleview_column()
                    .update_share_popover();
            }
            Propagation::Proceed
        });

        imp.share_clipboard_switch.connect_state_set(|_switch, is_set| {
            if App::default()
                .settings()
                .write()
                .set_share_clipboard_enabled(is_set)
                .is_err()
            {
                App::default().in_app_notifiaction(&i18n("Failed to set setting 'clipboard enabled'"));
            } else {
                App::default()
                    .main_window()
                    .content_page()
                    .articleview_column()
                    .update_share_popover();
            }
            Propagation::Proceed
        });

        imp.share_custom_switch.connect_state_set(clone!(
            @weak self as this => @default-panic, move |_switch, is_set| {

            let imp = this.imp();
            imp.share_custom_name_entry.set_sensitive(is_set);
            imp.share_custom_url_entry.set_sensitive(is_set);

            if App::default()
                .settings()
                .write()
                .set_share_custom_enabled(is_set)
                .is_err()
            {
                App::default().in_app_notifiaction(&i18n("Failed to set setting 'custom share enabled'"));
            } else {
                App::default()
                    .main_window()
                    .content_page()
                    .articleview_column()
                    .update_share_popover();
            }
            Propagation::Proceed
        }));

        imp.share_custom_name_entry.connect_text_notify(|entry| {
            if App::default()
                .settings()
                .write()
                .set_share_custom_name(Some(entry.text().as_str().into()))
                .is_err()
            {
                App::default().in_app_notifiaction(&i18n("Failed to set setting 'custom share name'"));
            } else {
                App::default()
                    .main_window()
                    .content_page()
                    .articleview_column()
                    .update_share_popover();
            }
        });

        imp.share_custom_url_entry.connect_text_notify(|entry| {
            if App::default()
                .settings()
                .write()
                .set_share_custom_url(Some(entry.text().as_str().into()))
                .is_err()
            {
                App::default().in_app_notifiaction(&i18n("Failed to set setting 'custom share url'"));
            } else {
                App::default()
                    .main_window()
                    .content_page()
                    .articleview_column()
                    .update_share_popover();
            }
        });
    }
}
