use crate::util::{constants, GtkUtil};
use glib::clone;
use gtk4::{prelude::*, subclass::prelude::*, Align, Box, CompositeTemplate, Label};
use news_flash::models::Tag;

mod imp {
    use gtk4::DrawingArea;

    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/article_list/row_tags.blp")]
    pub struct ArticleRowTags {
        #[template_child]
        pub full_names: TemplateChild<Box>,
        #[template_child]
        pub color_circles: TemplateChild<DrawingArea>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ArticleRowTags {
        const NAME: &'static str = "ArticleRowTags";
        type ParentType = gtk4::Box;
        type Type = super::ArticleRowTags;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ArticleRowTags {}

    impl WidgetImpl for ArticleRowTags {}

    impl BoxImpl for ArticleRowTags {}
}

glib::wrapper! {
    pub struct ArticleRowTags(ObjectSubclass<imp::ArticleRowTags>)
        @extends gtk4::Widget, gtk4::Box;
}

impl Default for ArticleRowTags {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl ArticleRowTags {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn update_tags(&self, tags: &[Tag]) {
        self.create_labels(tags);
        self.create_color_circles(tags);
    }

    fn create_color_circles(&self, tags: &[Tag]) {
        let imp = self.imp();

        let colors = tags
            .iter()
            .map(|tag| tag.color.as_deref().unwrap_or(constants::TAG_DEFAULT_COLOR).to_owned())
            .collect::<Vec<_>>();

        imp.color_circles.set_draw_func(
            clone!(@strong colors => @default-panic, move |_drawing_area, ctx, width, height| {
                let circle_count = colors.len() as f64;
                let size = 16_f64;
                let margin = 2_f64;
                let half_size = size / 2_f64;
                let min_offset = half_size;
                let max_offset = size + margin;
                let offset = width as f64 / circle_count;
                let offset = offset.min(max_offset);
                let offset = offset.max(min_offset);

                for (i, color) in colors.iter().enumerate() {
                    let center_x = width as f64 - (half_size + 2.0 * margin) - (offset * i as f64);
                    let center_y = height as f64 / 2.0;
                    GtkUtil::draw_color_cirlce(ctx, color, Some((center_x, center_y)));
                }
            }),
        );
        imp.color_circles.queue_draw();
    }

    fn create_labels(&self, tags: &[Tag]) {
        let imp = self.imp();

        while let Some(label) = imp.full_names.first_child() {
            imp.full_names.remove(&label);
        }

        for tag in tags {
            let label = Self::create_tag_label(tag);
            imp.full_names.append(&label);
        }
    }

    fn create_tag_label(tag: &Tag) -> Label {
        let css_selector = tag
            .tag_id
            .as_str()
            .chars()
            .filter(|c| c.is_alphanumeric() && !c.is_whitespace())
            .collect::<String>();
        let css_selector = format!("tag-style-{css_selector}");
        let label = Label::new(Some(&tag.label));
        label.set_margin_start(2);
        label.set_margin_end(2);
        label.set_valign(Align::Center);
        label.add_css_class(&css_selector);
        label.add_css_class("subtitle");
        label
    }
}
