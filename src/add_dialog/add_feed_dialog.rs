use crate::add_dialog::g_parsed_url::GParsedUrl;
use crate::add_dialog::{
    add_error_widget::AddErrorWidget, add_feed_widget::AddFeedWidget, parse_feed_widget::ParseFeedWidget,
    select_feed_widget::SelectFeedWidget,
};
use glib::{clone, subclass};
use gtk4::{prelude::*, subclass::prelude::*, CompositeTemplate, Widget};
use libadwaita::{prelude::*, subclass::prelude::*, Dialog, NavigationView};
use news_flash::models::Feed;
use news_flash::ParsedUrl;

use super::g_feed::GFeed;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/add_dialog/feed.blp")]
    pub struct AddFeedDialog {
        #[template_child]
        pub navigation_view: TemplateChild<NavigationView>,

        #[template_child]
        pub parse_feed_widget: TemplateChild<ParseFeedWidget>,
        #[template_child]
        pub select_feed_widget: TemplateChild<SelectFeedWidget>,
        #[template_child]
        pub add_feed_widget: TemplateChild<AddFeedWidget>,
        #[template_child]
        pub error_widget: TemplateChild<AddErrorWidget>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for AddFeedDialog {
        const NAME: &'static str = "AddFeedDialog";
        type ParentType = Dialog;
        type Type = super::AddFeedDialog;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for AddFeedDialog {}

    impl WidgetImpl for AddFeedDialog {}

    impl AdwDialogImpl for AddFeedDialog {}
}

glib::wrapper! {
    pub struct AddFeedDialog(ObjectSubclass<imp::AddFeedDialog>)
        @extends Widget, Dialog;
}

impl Default for AddFeedDialog {
    fn default() -> Self {
        Self::new()
    }
}

impl AddFeedDialog {
    pub fn new() -> Self {
        let dialog = glib::Object::new::<Self>();

        let imp = dialog.imp();

        imp.parse_feed_widget.connect_local(
            "feed-parsed",
            false,
            clone!(@weak dialog => @default-panic, move |args| {
                let g_parsed_url = args[1].get::<GParsedUrl>()
                    .expect("The value needs to be of type `GParsedUrl`.");
                let parsed_url: ParsedUrl = g_parsed_url.into();

                let imp = dialog.imp();
                match parsed_url {
                    ParsedUrl::SingleFeed(feed) => {
                        imp.add_feed_widget.fill(*feed);
                        imp.navigation_view.push_by_tag("feed_add_page");
                    },
                    ParsedUrl::MultipleFeeds(feed_vec) => {
                        imp.select_feed_widget.fill(feed_vec);
                        imp.navigation_view.push_by_tag("feed_selection_page");
                    },
                }
                None
            }),
        );

        imp.parse_feed_widget.connect_local(
            "error",
            false,
            clone!(@weak dialog => @default-panic, move |args| {
                let error_msg = args[1].get::<String>()
                    .expect("The value needs to be of type `String`.");

                let imp = dialog.imp();
                imp.error_widget.set_error(&error_msg);
                imp.navigation_view.push_by_tag("error_page");
                None
            }),
        );

        imp.select_feed_widget.connect_local(
            "selected",
            false,
            clone!(@weak dialog => @default-panic, move |args| {
                let g_feed = args[1].get::<GFeed>()
                    .expect("The value needs to be of type `GFeed`.");
                let feed: Feed = g_feed.into();

                let imp = dialog.imp();
                imp.add_feed_widget.reset();
                imp.add_feed_widget.fill(feed);
                imp.navigation_view.push_by_tag("feed_add_page");
                None
            }),
        );

        imp.select_feed_widget.connect_local(
            "error",
            false,
            clone!(@weak dialog => @default-panic, move |args| {
                let error_msg = args[1].get::<String>()
                    .expect("The value needs to be of type `String`.");

                let imp = dialog.imp();
                imp.error_widget.set_error(&error_msg);
                imp.navigation_view.push_by_tag("error_page");
                None
            }),
        );

        imp.error_widget.connect_local(
            "try-again",
            false,
            clone!(@weak dialog => @default-panic, move |_| {
                let imp = dialog.imp();
                imp.parse_feed_widget.reset();
                imp.navigation_view.pop_to_tag("feed_parse_page");
                None
            }),
        );

        imp.add_feed_widget.connect_local(
            "feed-added",
            false,
            clone!(@weak dialog => @default-panic, move |_| {
                dialog.close();
                None
            }),
        );

        imp.navigation_view.replace_with_tags(&["feed_parse_page"]);

        dialog
    }

    pub fn parse_url_str(&self, url: &str) {
        self.imp().parse_feed_widget.parse_url_str(url);
    }
}
