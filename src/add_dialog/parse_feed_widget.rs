use std::sync::Arc;

use crate::add_dialog::g_parsed_url::GParsedUrl;
use crate::app::App;
use crate::i18n::i18n;
use glib::{clone, subclass, subclass::*};
use gtk4::{prelude::*, subclass::prelude::*, Box, Button, CompositeTemplate, Stack, Widget};
use libadwaita::prelude::EntryRowExt;
use libadwaita::EntryRow;
use once_cell::sync::Lazy;

use news_flash::models::{FeedID, Url};
use tokio::sync::Semaphore;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/add_dialog/parse_feed.blp")]
    pub struct ParseFeedWidget {
        #[template_child]
        pub stack: TemplateChild<Stack>,
        #[template_child]
        pub url_entry: TemplateChild<EntryRow>,
        #[template_child]
        pub parse_button: TemplateChild<Button>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ParseFeedWidget {
        const NAME: &'static str = "ParseFeedWidget";
        type ParentType = Box;
        type Type = super::ParseFeedWidget;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ParseFeedWidget {
        fn signals() -> &'static [Signal] {
            static SIGNALS: Lazy<Vec<Signal>> = Lazy::new(|| {
                vec![
                    Signal::builder("feed-parsed")
                        .param_types([GParsedUrl::static_type()])
                        .build(),
                    Signal::builder("error").param_types([String::static_type()]).build(),
                ]
            });
            SIGNALS.as_ref()
        }

        fn constructed(&self) {
            self.obj().init();
        }
    }

    impl WidgetImpl for ParseFeedWidget {}

    impl BoxImpl for ParseFeedWidget {}
}

glib::wrapper! {
    pub struct ParseFeedWidget(ObjectSubclass<imp::ParseFeedWidget>)
        @extends Widget, Box;
}

impl Default for ParseFeedWidget {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl ParseFeedWidget {
    pub fn new() -> Self {
        Self::default()
    }

    fn init(&self) {
        let imp = self.imp();

        // make parse button sensitive if entry contains text and vice versa
        let parse_button = imp.parse_button.get();
        imp.url_entry
            .connect_changed(clone!(@weak parse_button => @default-panic, move |entry| {
                if entry.text().as_str().is_empty() {
                    parse_button.set_sensitive(false);
                } else {
                    parse_button.set_sensitive(true);
                }
            }));

        // hit enter in entry to parse url
        imp.url_entry
            .connect_entry_activated(clone!(@weak parse_button => @default-panic, move |_entry| {
                if parse_button.get_sensitive() {
                    parse_button.emit_clicked();
                }
            }));

        // parse url and switch to feed selection or final page
        imp.parse_button.connect_clicked(clone!(
            @weak self as widget => @default-panic, move |_button|
        {
            let imp = widget.imp();

            let mut url_text = imp.url_entry.text().as_str().to_owned();
            if !url_text.starts_with("http://") && !url_text.starts_with("https://") {
                url_text.insert_str(0, "https://");
            }
            if let Ok(url) = Url::parse(&url_text) {
                widget.parse_feed_url(&url);
            } else {
                log::error!("No valid url: '{}'", url_text);
                widget.reset();
                widget.emit_by_name::<()>("error", &[&i18n("Not a valid URL")]);
            }
        }));
    }

    pub fn reset(&self) {
        let imp = self.imp();
        imp.url_entry.set_text("");
        imp.parse_button.set_sensitive(false);
        imp.stack.set_visible_child_name("widget");
    }

    pub fn parse_url_str(&self, url: &str) {
        let imp = self.imp();
        imp.url_entry.set_text(url);
        imp.parse_button.set_sensitive(true);
        imp.parse_button.emit_clicked();
    }

    fn parse_feed_url(&self, url: &Url) {
        let imp = self.imp();

        // set 'next' button insensitive and show spinner
        imp.stack.set_visible_child_name("spinner_page");
        imp.parse_button.set_sensitive(false);

        let feed_id = FeedID::new(url.as_str());
        let url_clone = url.clone();

        App::default().execute_with_callback(
            |news_flash, client| async move {
                let semaphore = news_flash
                    .read()
                    .await
                    .as_ref()
                    .map(|x| x.get_semaphore())
                    .unwrap_or(Arc::new(Semaphore::new(1)));
                news_flash::feed_parser::download_and_parse_feed(&url_clone, &feed_id, None, semaphore, &client).await
            },
            clone!(@weak self as widget, @strong url => @default-panic, move |_app, res| {
                match res {
                    Ok(parsed_url) => {
                        widget.emit_by_name::<()>("feed-parsed", &[&GParsedUrl::from(parsed_url)]);
                        widget.imp().stack.set_visible_child_name("widget");
                    },
                    Err(error) => {
                        log::error!("No feed found for url '{}': {}", url, error);
                        widget.reset();
                        widget.emit_by_name::<()>("error", &[&i18n("No Feed found")]);
                    }
                }
            }),
        );
    }
}
