use crate::app::App;
use glib::{clone, subclass};
use gtk4::{prelude::*, subclass::prelude::*, Button, CompositeTemplate, Widget};
use libadwaita::{prelude::*, Dialog, EntryRow};

mod imp {
    use libadwaita::subclass::dialog::AdwDialogImpl;

    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/add_dialog/category.blp")]
    pub struct AddCategoryDialog {
        #[template_child]
        pub add_category_button: TemplateChild<Button>,
        #[template_child]
        pub category_entry: TemplateChild<EntryRow>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for AddCategoryDialog {
        const NAME: &'static str = "AddCategoryDialog";
        type ParentType = Dialog;
        type Type = super::AddCategoryDialog;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for AddCategoryDialog {}

    impl WidgetImpl for AddCategoryDialog {}

    impl AdwDialogImpl for AddCategoryDialog {}
}

glib::wrapper! {
    pub struct AddCategoryDialog(ObjectSubclass<imp::AddCategoryDialog>)
        @extends Widget, Dialog;
}

impl Default for AddCategoryDialog {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl AddCategoryDialog {
    pub fn new() -> Self {
        let dialog = Self::default();
        dialog.init();
        dialog
    }

    fn init(&self) {
        let imp = self.imp();

        let add_category_button = imp.add_category_button.get();
        let category_entry = imp.category_entry.get();

        // make parse button sensitive if entry contains text and vice versa
        imp.category_entry
            .connect_changed(clone!(@weak add_category_button => @default-panic, move |entry| {
                add_category_button.set_sensitive(!entry.text().as_str().is_empty());
            }));

        // hit enter in entry to add category
        imp.category_entry
            .connect_entry_activated(clone!(@weak add_category_button => @default-panic, move |_entry| {
                if add_category_button.get_sensitive() {
                    add_category_button.emit_clicked();
                }
            }));

        imp.add_category_button.connect_clicked(clone!(
            @weak self as widget,
            @weak category_entry => @default-panic, move |_button|
        {
            if !category_entry.text().as_str().is_empty() {
                App::default().add_category(category_entry.text().as_str().into());
                widget.close();
            }
        }));
    }
}
