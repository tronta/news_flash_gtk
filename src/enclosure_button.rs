use crate::app::App;
use crate::i18n::i18n;
use crate::image_dialog::ImageDialog;
use crate::util::constants::YOUTUBE_NEEDLE;
use crate::{enclosure_popover::EnclosurePopover, util::GtkUtil};
use gio::{AppInfo, AppLaunchContext};
use glib::SignalHandlerId;
use glib::{clone, subclass};
use gtk4::{prelude::*, subclass::prelude::*, Box, Button, CompositeTemplate, MenuButton, Stack, Widget};
use libadwaita::prelude::*;
use news_flash::models::Enclosure;
use std::cell::RefCell;

mod imp {
    use super::*;

    #[derive(Debug, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/enclosures/button.blp")]
    pub struct EnclosureButton {
        #[template_child]
        pub stack: TemplateChild<Stack>,
        #[template_child]
        pub single_button: TemplateChild<Button>,
        #[template_child]
        pub multiple_button: TemplateChild<MenuButton>,
        #[template_child]
        pub popover: TemplateChild<EnclosurePopover>,

        pub single_button_signal_id: RefCell<Option<SignalHandlerId>>,
    }

    impl Default for EnclosureButton {
        fn default() -> Self {
            Self {
                stack: TemplateChild::default(),
                single_button: TemplateChild::default(),
                multiple_button: TemplateChild::default(),
                popover: TemplateChild::default(),

                single_button_signal_id: RefCell::new(None),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for EnclosureButton {
        const NAME: &'static str = "EnclosureButton";
        type Type = super::EnclosureButton;
        type ParentType = Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for EnclosureButton {}

    impl WidgetImpl for EnclosureButton {}

    impl BoxImpl for EnclosureButton {}
}

glib::wrapper! {
    pub struct EnclosureButton(ObjectSubclass<imp::EnclosureButton>)
        @extends Widget, Box;
}

impl EnclosureButton {
    pub fn update(&self, enclosures: &[Enclosure]) {
        let imp = self.imp();

        if enclosures.len() == 1 {
            imp.stack.set_visible_child_name("single");

            GtkUtil::disconnect_signal(imp.single_button_signal_id.take(), &*imp.single_button);

            let enclosure = enclosures[0].clone();

            let (icon_name, tooltip) = if let Some(mime) = enclosure.mime_type.as_deref() {
                (Self::icon_from_mime(mime), Self::tooltip_from_mime(mime))
            } else if enclosure.url.as_str().contains(YOUTUBE_NEEDLE) {
                ("video-x-generic-symbolic", i18n("Watch Youtube Video"))
            } else {
                ("globe-alt-symbolic", i18n("Open Attachment"))
            };
            imp.single_button.set_tooltip_text(Some(&tooltip));
            imp.single_button.set_icon_name(icon_name);

            imp.single_button_signal_id
                .replace(Some(imp.single_button.connect_clicked(
                    clone!(@strong enclosure => @default-panic, move |_button| Self::launch_enclosure(&enclosure)),
                )));
        } else {
            imp.stack.set_visible_child_name("multiple");
            imp.popover.update(enclosures);
        }
    }

    pub fn launch_enclosure(enclosure: &Enclosure) {
        let url_str = enclosure.url.as_str();

        if let Some(mime) = enclosure.mime_type.as_ref() {
            if mime.starts_with("image") {
                ImageDialog::new_url(&enclosure.article_id, &enclosure.url).present(&App::default().main_window());
                return;
            } else if mime.starts_with("video") {
                App::default().main_window().play_enclosure(enclosure);
                return;
            }
        }

        if enclosure.url.as_str().contains(YOUTUBE_NEEDLE) {
            App::default().main_window().play_enclosure(enclosure);
            return;
        }

        // FIXME: hope at some point opening default app for mime-type will be possible with desktop-portal
        // https://github.com/flatpak/xdg-desktop-portal/issues/574
        if let Err(error) = AppInfo::launch_default_for_uri(url_str, None::<&AppLaunchContext>) {
            log::warn!(
                "Failed to launch default application for uri: '{}', {error}",
                enclosure.url
            );
        }
    }

    fn icon_from_mime(mime: &str) -> &'static str {
        if mime.starts_with("image") {
            "image-x-generic-symbolic"
        } else if mime.starts_with("video") || mime == "application/x-shockwave-flash" {
            "video-x-generic-symbolic"
        } else if mime.starts_with("audio") {
            "audio-x-generic-symbolic"
        } else {
            "globe-alt-symbolic"
        }
    }

    fn tooltip_from_mime(mime: &str) -> String {
        if mime.starts_with("image") {
            i18n("Open Attached Image")
        } else if mime.starts_with("video") {
            i18n("Open Attached Video")
        } else if mime.starts_with("audio") {
            i18n("Watch Youtube Video")
        } else {
            i18n("Open Attachment")
        }
    }
}
