use thiserror::Error;

#[derive(Error, Debug)]
pub enum UtilError {
    #[error("IO Error")]
    IO(#[from] glib::Error),
    #[error("Error (de)serializing an object")]
    Json(#[from] serde_json::Error),
    #[error("Failed to parse svg data")]
    Svg,
    #[error("Error sending HTTP request")]
    Network(#[from] reqwest::Error),
}
